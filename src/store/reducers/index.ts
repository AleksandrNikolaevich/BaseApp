import { combineReducers } from 'redux';
/* import auth from "./AuthReducer";*/
import app, {AppState as AppReducer} from "./AppReducer"; 
import auth, {AuthState} from "./AuthReducer"; 
export declare type AppState = {
    app: AppReducer,
    auth: AuthState,
}

//тут мы соединяем все редюсеры
export default combineReducers({
    app,
    auth,
});