
import { setSettings, setLaunchAppState, AppActions } from "../actions/AppActions";
import {
    Config
} from "../../config"
import { TLaunchState } from "../../types";
//начальное состояние для этого редюсера

export declare type AppState = {
    settings?: Config,
    launchAppState: TLaunchState
}

const initialState: AppState = {
    settings: undefined,
    launchAppState: "pending"
};



const appReducer = (state = initialState, action: AppActions): AppState => {
    let newState;
    switch (action.type) {
        case setSettings:
            //новое состояние = старое состяние  + поменяли свойство
            // конструкция аналогичная newState = Object.assign({}, state, {loading: action.state});
            newState = { ...state, settings: action.settings };
            break;
        case setLaunchAppState:
            newState = { ...state, launchAppState: action.state };
            break;
    }
    return newState || state;
};

export default appReducer;