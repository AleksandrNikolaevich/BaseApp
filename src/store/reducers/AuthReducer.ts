
import { AUTH_TOGGLE_PROCESS, AUTH_SUCCESS, AuthActions } from "../actions/AuthActions";

//начальное состояние для этого редюсера

export declare type AuthState = {
    user: any, //TODO: описать тип юзера
    loading: boolean
}

const initialState: AuthState = {
    user: null,
    loading: false
};



const appReducer = (state = initialState, action: AuthActions): AuthState => {
    let newState;
    switch (action.type) {
        case AUTH_TOGGLE_PROCESS:
            newState = { ...state, loading: action.state };
            break;
        case AUTH_SUCCESS:
            newState = { ...state, user: action.user };
            break;
    }
    return newState || state;
};

export default appReducer;