
import { ThunkAction } from "redux-thunk";
import { AppState } from "../reducers";
import SubmissionError from "../../utils/exceptions/SubmissionError";
import { FORM_ERROR } from "final-form";
import { ThunkExtra } from "../../utils/redux";
import NavigationService from "../../utils/NavigationService";

export const AUTH_TOGGLE_PROCESS = "AUTH_TOGGLE_PROCESS";
export const AUTH_SUCCESS = "AUTH_SUCCESS";

export interface Login { type: typeof AUTH_SUCCESS, user: any }
export interface ToggleProgress { type: typeof AUTH_TOGGLE_PROCESS, state: boolean }

export const identityStoreKey = "userIdentity"

export interface RegisterValues {
    fio: string,
    email: string,
    phone: string,
    password: string,
}

type LoginAction = Login | ToggleProgress;
type RecoveryPasswordAction = ToggleProgress;
type SignupAction = ToggleProgress;

export type AuthActions = LoginAction | RecoveryPasswordAction | SignupAction;

export type WithAuthActions = {
    login: (login: string, password: string) => ThunkAction<void, AppState, ThunkExtra, LoginAction>,
    singup: (values: RegisterValues) => ThunkAction<void, AppState, ThunkExtra, LoginAction>,
    recoveryPassword: (email: string) => ThunkAction<void, AppState, ThunkExtra, SignupAction>
}

const toggleProcess = (state: boolean): ToggleProgress => {
    return {
        type: AUTH_TOGGLE_PROCESS,
        state
    }
};


const AuthActions: WithAuthActions = {
    login(login: string, password: string) {
        return async (dispatch, getState, { api }) => {
            const { settings } = getState().app;
            dispatch(toggleProcess(true));
            try {
                const response = await api.fetch.post(settings!.api.login, {
                    login: login,
                    password: password,
                });
                console.log(response);
                /* if(response.status === 200 ){
                    //успешный обрабатываем тут 

                    await AsyncStorage.setItem(identityStoreKey, JSON.stringify(response));
                        dispatch({
                            type: AUTH_SUCCESS,
                            user: response,
                        });    
                } */

                //если были ошибки валидации то кидаем ошибку
                if(login && password){
                    NavigationService.navigate('Main')
                }
                throw new SubmissionError({
                    [FORM_ERROR]: "Общая ошибка формы", //не обязательна
                    login: "Ошибка для поля логин",
                    password: "Ошибка для поля password"
                })

                dispatch(toggleProcess(false));

                return null;
            } catch (error) {
                dispatch(toggleProcess(false));
                if (error instanceof SubmissionError) {
                    throw error
                }
                console.error(error);
            }
        }
    },
    singup(values: RegisterValues) {
        return async (dispatch, getState, { api }) => {
            const { settings } = getState().app;
            dispatch(toggleProcess(true));
            try {
                const response = await api.fetch.post(settings!.api.singup, {
                    values,
                });


                /* if(response.status === 200 ){
                    //успешный обрабатываем тут  
                } */

                //если были ошибки валидации то кидаем ошибку
                throw new SubmissionError({
                    [FORM_ERROR]: "Общая ошибка формы", //не обязательна
                    fio: "Вы указали невалидный емейл",
                    email: "Вы указали невалидный емейл",
                    phone: "Вы указали невалидный емейл",
                    password: "Вы указали невалидный емейл"
                })

                dispatch(toggleProcess(false));

                return null;
            } catch (error) {
                dispatch(toggleProcess(false));
                if (error instanceof SubmissionError) {
                    throw error
                }
                console.error(error);
            }
        }
    },
    recoveryPassword(email: string) {
        return async (dispatch, getState, { api }) => {
            const { settings } = getState().app;
            dispatch(toggleProcess(true));
            try {

                const response = await api.fetch.post(settings!.api.recoveryPassword, {
                    email,
                });
                

                /* if(response.status === 200 ){
                    //успешный обрабатываем тут  
                } */

                //если были ошибки валидации то кидаем ошибку
                throw new SubmissionError({
                    [FORM_ERROR]: "Общая ошибка формы", //не обязательна
                    email: "Вы указали невалидный емейл"
                })

                dispatch(toggleProcess(false));

                return null;
            } catch (error) {
                dispatch(toggleProcess(false));
                if (error instanceof SubmissionError) {
                    throw error
                }
                console.error(error);
            }
        }
    }
}

export default AuthActions;