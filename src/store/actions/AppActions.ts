import {
    AsyncStorage, AppState
} from "react-native";
import { config, Config } from "../../config";
import { Dispatch } from "redux";
import { ThunkAction } from "redux-thunk";
import { ThunkExtra } from "../../utils/redux";
import { TLaunchState } from "../../types";

export const setSettings = 'SET_SETTINGS';
export const setLaunchAppState = "SET_LOUNCH_APP_STATE";



export interface InitSettings { type: typeof setSettings, settings: Config }
export interface SetLaunchAppState { type: typeof setLaunchAppState, state: TLaunchState }

export type AppActions = InitSettings | SetLaunchAppState;

export type WithAppActions = {
    initSettings: () => ThunkAction<void, AppState, ThunkExtra, InitSettings>,
    setLaunchAppState: (state: TLaunchState) => SetLaunchAppState,
}

const settingsKeyStore: string = 'settings';



const AppActions: WithAppActions = {
    initSettings: () => {
        return async (dispatch, getState, { api }) => {
            await AsyncStorage.setItem(settingsKeyStore, JSON.stringify(config));
            api.setBaseUrl(config.baseUrl);
            dispatch({
                type: setSettings,
                settings: config
            })
        }
    },
    setLaunchAppState: (state: TLaunchState) => {
        return {
            type: setLaunchAppState,
            state
        }
    }
}

export default AppActions;