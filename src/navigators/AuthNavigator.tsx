import * as React from "react";
import Login from "../screens/auth/screens/Login";
import Registration from "../screens/auth/screens/Registration";
import PasswordRecovery from "../screens/auth/screens/PasswordRecovery";
import {createStackNavigator} from 'react-navigation';

export default createStackNavigator({
    Login: { 
        screen: Login,
        navigationOptions: () => ({
            title: `Вход`,
          }),
    },
    Registration: { 
        screen: Registration,
        navigationOptions: () => ({
            title: `Регистрация`,
          }),
    },
    PasswordRecovery: { 
        screen: PasswordRecovery,
        navigationOptions: () => ({
            title: `Восстановление пароля`,
          }),
    }
});