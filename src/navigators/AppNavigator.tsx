import * as React  from "react";
import { createSwitchNavigator, NavigationState, createAppContainer, NavigationNavigator, NavigationComponent, NavigationScreenProps } from 'react-navigation';
import { KeyboardAvoidingView, Platform, StyleSheet, View, Image } from 'react-native';
import Auth from "./AuthNavigator";
import LaunchScreen from "../screens/launch/LaunchScreen";
import Splash from "../screens/launch/Splash";
import NavigationService from "../utils/NavigationService";
import MainNavigator from "./MainNavigator";



//для основной части приложения, той что скрыта от неавторизованного юзера выбираем навигацию типа стек
// тут могут быть и табики и левое меню и д.р

export function getCurrentRouteName(navigationState: NavigationState): string | null {
    if (!navigationState) {
        return null;
    }
    const route: any = navigationState.routes[navigationState.index]; //NavigationRoute
    // dive into nested navigators
    if (route.routes) {
        return getCurrentRouteName(route);
    }
    return route.routeName;
}



/* const MainTab = TabNavigator({
     Catalog: {
        screen: Catalogs,
        navigationOptions: {
            tabBarLabel: 'Каталог',
            tabBarIcon: ({focused}) => {
                let iconName;
                iconName = focused ?
                    require('../assets/image/tabNavigation/active/каталог.png') :
                    require('../assets/image/tabNavigation/noActive/каталог.png');
                return <Image source={iconName} style={{width: 19, height: 25}}/>;
            }
        },
    },
    History: {
        screen: Historys,
        navigationOptions: {
            tabBarLabel: 'История',
            tabBarIcon: ({ focused }) => {
                let iconName;
                iconName = focused ?
                    require('../assets/image/tabNavigation/active/историяЗаказов.png') :
                    require('../assets/image/tabNavigation/noActive/историяЗаказов.png');
                return <Image source={iconName} style={{width: 25, height: 25}}/>;
            }
        },
    },
    Stock: Stock,
    Profile: {
        screen: Profiles,
        navigationOptions: {
            tabBarLabel: 'Профиль',
            tabBarIcon: ({focused}) => {
                let iconName;
                iconName = focused ?
                    require('../assets/image/tabNavigation/active/профиль.png') :
                    require('../assets/image/tabNavigation/noActive/профиль.png');
                return <Image source={iconName} style={{width: 25, height: 25}}/>;
            }
        },
    } 
},
{

    swipeEnabled:false,
    tabBarComponent: (props)=>{
        const currentRoute = getCurrentRouteName(props.navigationState);
        if( currentRoute === 'Basket' || currentRoute === 'Ordering'){
            return <TabBarBottom {...props} style={{
                backgroundColor: COLOR.default,
                height: 55,
                padding: 5,
                borderTopWidth: 1,
                borderColor: COLOR.grayLighter,

            }}/>
        } 
        return <TabBarBottom {...props}/>
    },
    tabBarPosition: 'bottom',
    tabBarOptions: {
        style: {
            backgroundColor: COLOR.default,
            height: 55,
            padding: 5,
            borderTopWidth: 0,

            shadowColor: COLOR.black,
            shadowOffset: { width: 0, height:0},
            shadowOpacity: .2,
            shadowRadius: 10,
            elevation:20,

        },
        tabStyle: {
            backgroundColor: COLOR.default,
        }
    } 
}
); */

/* const Main = StackNavigator({
    Main: { 
        screen: MainTab, 
        navigationOptions:{ header: null } 
    },
    //Ordering: Ordering //тут модальные роуты
}); */



//switcher navigators
//позволяет переключаться между навигаторами, читай документацию
export const Navigator = createAppContainer(createSwitchNavigator(
    {
        Launch: LaunchScreen,
        Auth: Auth,
        Main: MainNavigator
    },
    {
        initialRouteName: 'Launch',
    }
));




class AppNavigator extends React.PureComponent {

    render() {
        //KeyboardAvoidingView нужен для иммитации поведения клавиатуры как на адройде для иос
        const Container = Platform.OS === 'ios' ? KeyboardAvoidingView : View;
        //SafeAreaView нужно для айфонов Х
        return (
            <Container style={{ flex: 1 }} behavior="padding" enabled>
                <Navigator
                    ref={NavigationService.setTopLevelNavigator}
                />
                <Splash />
            </Container>
            /*<SafeAreaView
                style={{flex: 1}}
                forceInset={{top: 'always', horizontal: 'never'}}
            >

            </SafeAreaView>*/
        )
    }
}


export default AppNavigator;