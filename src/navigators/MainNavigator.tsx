import * as React from "react";
import { createStackNavigator, NavigationScreenProps } from 'react-navigation';
import {StyleSheet, Image, ViewStyle, ImageStyle, Keyboard, StatusBar, Platform} from 'react-native';
import { connect } from "react-redux";
import Catalog from "../screens/main/catalog/screens/Catalog";
import History from "../screens/main/history/screens/History";
import Promotions from "../screens/main/promotions/screens/Promotions";
//@ts-ignore
import { createBottomTabNavigator, BottomTabBar } from 'react-navigation-tabs';
import { withNotify, WithNotify } from "react-native-app-notification";
import { Images } from "../assets/Images";
import { getCurrentRouteName } from "./AppNavigator";
import Badge from "../components/Badge";
import { colors } from "../assets/styles/DefaultTheme";




const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    icon: {
        width: 23,
        height: 23
    }
})



const MainTab = createBottomTabNavigator({
    Catalog: {
        screen: Catalog,
        navigationOptions: ({ navigation }: any) => ({
            tabBarLabel: 'Каталог',
            tabBarIcon: (props: any) => {
                let iconName;
                iconName = props.focused ?
                    Images.mainNavigation.catalog.active:
                    Images.mainNavigation.catalog.inactive;
                return <Image resizeMode={'contain'} source={iconName} style={styles.icon} />;
            }
        }),
    },
    History: {
        screen: History,
        navigationOptions: {
            tabBarLabel: 'История',
            tabBarIcon: ({ focused }: any) => {
                let iconName;
                iconName = focused ?
                    Images.mainNavigation.history.active :
                    Images.mainNavigation.history.inactive;
                return <Image resizeMode={'contain'} source={iconName} style={styles.icon} />;
            }
        },
    },
    Stock: {
        screen: Promotions,
        navigationOptions: ({ navigation }: any) => {
            const hasNewPromotions = !!navigation.state.params && navigation.state.params.countPromotions > 0;
            return {
                tabBarLabel: 'Акции',
                tabBarIcon: ({ focused }: any) => {
                    let iconName;
                    iconName = focused ?
                        Images.mainNavigation.promotions.active :
                        Images.mainNavigation.promotions.inactive;
                    return (<Badge
                        value={hasNewPromotions ? navigation.state.params.countPromotions : 0}
                        hide={!hasNewPromotions}
                        top={0}
                        right={-7}
                    >
                        <Image resizeMode={'contain'} source={iconName} style={styles.icon} />
                    </Badge>);
                }
            }
        }
    },
},
    {

        swipeEnabled: false,
        tabBarComponent: (props: any) => {
            const currentRoute = getCurrentRouteName(props.navigationState);
            if (currentRoute === 'Basket') {
                // return <TabBarBottom {...props} style={{
                //     backgroundColor: COLOR.default,
                //     height: 55,
                //     padding: 5,
                //     borderTopWidth: 1,
                //     borderColor: COLOR.grayLighter,
                //     paddingHorizontal:15,
                // }} />
            }
            return <BottomTabBar {...props} />
        },
        tabBarPosition: 'bottom',
        tabBarOptions: {
            style: {
                backgroundColor: colors.footer,
                height: 55,
                padding: 5,
                borderTopWidth: 0,
                paddingHorizontal:15,
                shadowColor: 'black',
                shadowOffset: { width: 0, height: 0 },
                shadowOpacity: .2,
                shadowRadius: 10,
                elevation: 20,
            },
            tabStyle: {
                backgroundColor: colors.footer,
            },
            labelStyle: { fontSize: 14,},
        }
    }
);


const Main = createStackNavigator({
    Main: { screen: MainTab, navigationOptions: { header: null } },
   
});

const getParams = function (pushPayload: any) {
    let params = {}
    try {
        if (pushPayload.additionalData.params) {
            if (typeof pushPayload.additionalData.params === 'string') {
                params = JSON.parse(pushPayload.additionalData.params);
            }

            if (typeof pushPayload.additionalData.params === 'object') {
                params = pushPayload.additionalData.params;
            }
        }

    } catch (e) {
        params = {}
    }
    return params;
}

type Props = WithNotify
    & NavigationScreenProps

class MainNavigator extends React.PureComponent<Props, {}> {
    static router = Main.router;

    componentDidUpdate(prevProps: Props) {
        const { navigation } = this.props;
        //@ts-ignore
        if (getCurrentRouteName(navigation.state) !== getCurrentRouteName(prevProps.navigation.state)) {
            Keyboard.dismiss();
        }
    }

    componentDidMount() {
        // OneSignal.init(config.oneSignalKey, {
        //     kOSSettingsKeyAutoPrompt: true,
        //     kOSSettingsKeyInFocusDisplayOption: 0
        // });
        // // Check push notification and OneSignal subscription statuses
        // OneSignal.getPermissionSubscriptionState((status: any) => {
        //     this.props.setDeviceId(status.userId);
        //     console.log(status);
        // });
        // OneSignal.inFocusDisplaying(0);
        // OneSignal.addEventListener('received', this.onReceived);
        // OneSignal.addEventListener('opened', this.onOpened);
        // OneSignal.addEventListener('ids', this.onIds);
    }

    componentWillUnmount() {
        // OneSignal.removeEventListener('received', this.onReceived);
        // OneSignal.removeEventListener('opened', this.onOpened);
        // OneSignal.removeEventListener('ids', this.onIds);
    }

    // onReceived = (notification: any) => {
    //     console.log("Notification received: ", notification);
    //     if (notification.isAppInFocus) {
    //         const pushPayload = notification.payload;
    //         if (pushPayload.additionalData && pushPayload.additionalData.action === config.pushActions.goTo) {
    //             const routeName = pushPayload.additionalData.routeName;
    //             const params = getParams(pushPayload);
    //             if (this.props.navigation.state.routeName !== routeName) {
    //                 if (routeName === 'Delivered') {
    //                     this.props.navigation.navigate(routeName, params)
    //                 } else {
    //                     this.props.notify({
    //                         message: pushPayload.body,
    //                         onPress: () => { this.props.navigation.navigate(routeName, params) },
    //                         timeout: 10000
    //                     })
    //                 }

    //             }
    //         }
    //     }
    // }

    // onOpened = (openResult: any) => {

    //     const pushPayload = openResult.notification.payload;
    //     if (pushPayload.additionalData && pushPayload.additionalData.action === config.pushActions.goTo) {
    //         const routeName = pushPayload.additionalData.routeName;
    //         const params = getParams(pushPayload);
    //         this.props.navigation.dispatch(
    //             NavigationActions.navigate({ routeName, params })
    //         );
    //     }
    //     console.log('openResult: ', openResult);
    // }

    // onIds = (device: any) => {
    //     this.props.setDeviceId && this.props.setDeviceId(device.userId);
    //     //TODO: add sending device info on server
    //     console.log('Device info: ', device);
    // }

    render() {
        const { navigation } = this.props;

        return <Main navigation={navigation} />;
    }
}
const matStateToProps = () => {
    return {

    }
}

export default connect(matStateToProps)(withNotify(MainNavigator));