import * as React from "react";
import { NavigationScreenProps } from "react-navigation";
import Text from "../../../../components/Text";
import RootContainer from "../../../../components/RootContainer";


type OwnProps = {

}

type Props = NavigationScreenProps & OwnProps

class History extends React.PureComponent<Props,{}>{
    render(){
        return(
            <RootContainer>
                <Text>
                    History
                </Text>
            </RootContainer>
        )
    }
}

export default History