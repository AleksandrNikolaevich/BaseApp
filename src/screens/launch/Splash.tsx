import * as React from "react";
import {
    View,
    ActivityIndicator,
    ViewStyle,
    Text,
    ImageBackground,
    TextStyle,
    Animated,
    Image
} from "react-native";
import compose from "react-hoc-compose";
import { Images } from "../../assets/Images";
import { Theme } from "../../assets/styles/theme";
import { WithStyles, withStyles } from "react-native-theme-provider";
import { config } from "../../config";
import { AppState } from "../../store/reducers";
import { connect } from "react-redux";
import AppActions, { WithAppActions } from "../../store/actions/AppActions";
import { TLaunchState } from "../../types";
import LaunchState from "../../utils/enums/LaunchState";


type InjectedProps = {
    launchAppState: TLaunchState
}

type Props = WithStyles<Styles, Theme> 
    & WithAppActions
    & InjectedProps

interface Styles {
    root: ViewStyle,
    appName: TextStyle,
    desctiptionApp: TextStyle
}

const styles = (theme: Theme): Styles => ({
    root: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        zIndex: 1000000,
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        paddingHorizontal: 30
    },
    appName: {
        color: theme.colors.primary,
        
        ...theme.fonts.title,

        textAlign: 'center',
        height: 46 * config.appName.split('\n').length
    },
    desctiptionApp: {
        textAlign: 'center',
        color: theme.colors.primary,
        marginBottom: 20,
        ...theme.fonts.body
    }
})

type State = {
    showLaunchAnimationValue: any,
    closeLaunchAnimationValue: any,
    loading: boolean,
    appWasStarted: boolean
}

class LaunchScreen extends React.PureComponent<Props, State> {
    state: State = {
        showLaunchAnimationValue: new Animated.Value(0),
        closeLaunchAnimationValue: new Animated.Value(1),
        loading: false,
        appWasStarted: false
    }
    
    componentDidMount() {

        setTimeout(()=>{
            this.showLaunch();
        },700);

    }

    componentDidUpdate(prevProps: Props){
        const {
            launchAppState
        } = this.props;
        if(launchAppState !== prevProps.launchAppState && launchAppState === LaunchState.done){
            this.closeLaunch();
        }
    }


    closeLaunch = () =>{
        this.setState({loading: false})
        Animated.timing(this.state.closeLaunchAnimationValue, {
            duration: 1000,
            toValue: 0
        }).start(() => {
            this.setState({loading: false, appWasStarted: true});
        })
    }

    showLaunch = () => {
        Animated.timing(this.state.showLaunchAnimationValue, {
            duration: 500,
            toValue: 1
        }).start(() => {
            this.setState({loading: true});
            this.props.setLaunchAppState("launching")
        })
    }

    render() {
        const {
            styles,
        } = this.props;

        const {
            showLaunchAnimationValue,
            closeLaunchAnimationValue,
            loading,
            appWasStarted
        } = this.state;
        if(appWasStarted){
            return null;
        }
        return (
            <Animated.View 
                style={[
                    styles.root,
                    {
                        opacity: closeLaunchAnimationValue.interpolate({
                            inputRange:[0, 0.5, 1],
                            outputRange:[0, 1, 1]
                        }),
                    }
                ]}
            >
                <View style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <Animated.Image
                        source={Images.logo}
                        style={{ 
                            width: 100, 
                            height: 100, 
                            resizeMode: "contain",
                            transform:[{
                                scale: closeLaunchAnimationValue.interpolate({
                                    inputRange:[0, 0.5, 1],
                                    outputRange:[5, 0.8, 1]
                                }),
                            }]
                        }}
                    />
                    <Animated.View style={{
                        height: showLaunchAnimationValue.interpolate({
                            inputRange:[0, 1],
                            outputRange:[0, 100]
                        }),


                    }}>
                        <Animated.Text style={[
                            styles.appName,
                            {
                                opacity: showLaunchAnimationValue
                            }
                        ]}>{config.appName}</Animated.Text>
                        <Animated.Text style={[
                            styles.desctiptionApp,
                            {
                                opacity: showLaunchAnimationValue
                            }
                        ]}
                        numberOfLines={2}
                        >{config.desctiptionApp}</Animated.Text>
                        {
                            loading && <ActivityIndicator />
                        }
                    </Animated.View>

                </View>
            </Animated.View>
        )
    }
}

const mapStateToProps = (state:AppState) => ({
    launchAppState: state.app.launchAppState
})

export default compose(
    connect(mapStateToProps, AppActions),
    withStyles<Styles, Theme>(styles),
)(LaunchScreen);

