import * as React from "react";
import {
    View,
    ActivityIndicator,
    StyleSheet,
    ViewStyle
} from "react-native";
import {connect} from "react-redux"
import { AppState } from "../../store/reducers";
import { Dispatch} from "redux";
import compose from "react-hoc-compose";
import AppActions,  {WithAppActions}from "../../store/actions/AppActions";
import AuthActions, {WithAuthActions} from "../../store/actions/AuthActions";
import { AuthState } from "../../store/reducers/AuthReducer";
import { NavigationScreenProps } from "react-navigation";
import { TLaunchState } from "../../types";
import LaunchState from "../../utils/enums/LaunchState";

type Props = WithAppActions &
WithAuthActions &
NavigationScreenProps<{}> &
{
    auth: AuthState,
    app: AppState,
    launchAppState: TLaunchState
}

type Style = {
    root: ViewStyle
}

const styles = StyleSheet.create<Style>({
    root:{
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

class LaunchScreen extends React.PureComponent<Props,{}>{

    async componentDidMount(){
        await this.props.initSettings();
       
    }

    componentDidUpdate(prevProps: Props){
        const {
            launchAppState
        } =this.props;
        if(launchAppState !== prevProps.launchAppState && launchAppState === LaunchState.launching){
           this.initApp()
        }
    }

    initApp = () =>{
        setTimeout(()=>{
            this.props.setLaunchAppState("done")
        }, 2000)
        this.props.navigation.navigate("Auth")
    }

    render(){
        return(
            <View style={styles.root}>
                <ActivityIndicator/>
            </View>
        )
    }
}

const mapStateToProps = (state: AppState)=>{
    return{
        auth: state.auth,
        launchAppState: state.app.launchAppState
    }
}

const actions = {...AuthActions, ...AppActions}

export default compose(
    connect(mapStateToProps, actions)
)(LaunchScreen)
