import * as React from "react";
import { View, Text, Button, StyleSheet, TextStyle, ViewStyle } from "react-native";
import { NavigationScreenProps } from "react-navigation";
import { withNotify, WithNotify } from "react-native-app-notification";
import { Form, Field } from "react-final-form";
import FormHelper from "../../../utils/FormHelper";
import TextField from "../../../components/fields/TextField";
import ContainerWithLoader from "../../../components/ContainerWithLoader";
import compose from "react-hoc-compose";
import { connect } from "react-redux";
import { AppState } from "../../../store/reducers";
import AuthActions, { WithAuthActions } from "../../../store/actions/AuthActions";
import { AuthState } from "../../../store/reducers/AuthReducer";
import SubmissionError from "../../../utils/exceptions/SubmissionError";
import { Require } from "../../../utils/validators";
import { withStyles, WithStyles } from "react-native-theme-provider";
import { Theme, ThemeStyles } from "../../../assets/styles/theme";
import NavigationService from "../../../utils/NavigationService";


type AuthValues = {
    login: string,
    password: string
}

interface Styles {
    submitErrorMsg: TextStyle,
    centerContainer: ViewStyle,
}

const styles = (theme: Theme): Styles => ({
    submitErrorMsg: {
        color: theme.colors.error
    },
    centerContainer: {
        marginTop: 20,
        flexDirection: "row",
        justifyContent: 'center',
    }
})

interface IOwnProps {
    auth: AuthState
}

type Props = WithNotify &
    WithAuthActions &
    NavigationScreenProps<{}> &
    WithStyles<Styles, Theme> &
    IOwnProps;

class Login extends React.PureComponent<Props, {}>{



    async login(login: string, password: string) {
        try {
            await this.props.login(login, password);
            return null;
        } catch (e) {//SubmissionError
            return e.validationErrors

            /* 
            //если вдруг хотим обработать другую ошибку
             if(e instanceof SubmissionError){
                return e.validationErrors
            }
            //обработка другой ошибки
            return null; 
            */
        }
    }

    render() {

        const { notify, auth, styles, theme } = this.props

        return (
            <ContainerWithLoader
                showLoader={auth.loading}
                style={theme.styles.rootContainer}
            >
                <Form
                    onSubmit={async (values: any) => {
                        return await this.login(values.login, values.password);
                    }}
                    render={({ handleSubmit, submitError }) => {
                        return (
                            <View>
                                <Field
                                    name={'login'}
                                    render={(fieldProps) => {
                                        return (
                                            <TextField
                                                fieldProps={fieldProps}
                                                containerProps={{
                                                    style: {
                                                        marginTop: 20
                                                    }
                                                }}
                                                placeholder={"Логин"}
                                            />
                                        )
                                    }}
                                    validate={FormHelper.validate([
                                        new Require({ msg: 'Необходимо заполнить поле "E-mail"' })
                                    ])}
                                />
                                <Field
                                    name={'password'}
                                    render={(fieldProps) => {
                                        return (
                                            <TextField
                                                fieldProps={fieldProps}
                                                containerProps={{
                                                    style: {
                                                        marginTop: 20
                                                    }
                                                }}
                                                placeholder={"Пароль"}
                                            />
                                        )
                                    }}
                                    validate={FormHelper.validate([
                                        new Require({ msg: 'Необходимо заполнить поле "Пароль"' })
                                    ])}
                                />

                                <View style={{ paddingTop: 20, paddingRight: 20, alignSelf: 'flex-end' }}>
                                    <Text style={{ textAlign: 'right', color: 'blue' }}
                                        onPress={() => this.props.navigation.navigate('PasswordRecovery')}>Я забыл
                                                    пароль</Text>
                                </View>


                                <View style={{ paddingHorizontal: 60 }}>
                                    <Button
                                        title="Войти"
                                        onPress={() => handleSubmit()}
                                    />
                                </View>
                                {
                                    (!!submitError) &&
                                    <Text
                                        style={styles.submitErrorMsg}>{submitError}
                                    </Text>
                                }

                            </View>
                        )
                    }}
                />

                <View style={[styles.centerContainer]}>
                    <Text style={{ color: 'black' }}>В первый раз? </Text>
                    <Text style={{ color: 'blue' }}
                        onPress={() => NavigationService.navigate('Registration')}>
                        Регистрация </Text>
                </View>

            </ContainerWithLoader>
        )
    }
}

const mapStateToProps = (state: AppState) => {
    return {
        auth: state.auth
    }
}

const actions = {...AuthActions}

export default compose(
    withNotify,
    withStyles<Styles, Theme>(styles),
    connect(mapStateToProps, actions)
)(Login);