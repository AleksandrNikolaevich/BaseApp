import * as React from "react";
import { View, Text, Button, StyleSheet, TextStyle, ViewStyle } from "react-native";
import { NavigationScreenProps } from "react-navigation";
import { Form, Field } from "react-final-form";
import { connect } from "react-redux";
import TextField from "../../../components/fields/TextField";
import FormHelper from "../../../utils/FormHelper";
import ContainerWithLoader from "../../../components/ContainerWithLoader";
import AuthActions, { WithAuthActions } from "../../../store/actions/AuthActions";
import { AppState } from "../../../store/reducers";
import { AuthState } from "../../../store/reducers/AuthReducer";
import { Require, Email } from "../../../utils/validators";
import { Theme, ThemeStyles } from "../../../assets/styles/theme";
import { WithStyles, withStyles } from "react-native-theme-provider";
import compose from "react-hoc-compose";

type OwnProps = {
    auth: AuthState
}

interface Styles {
    submitErrorMsg: TextStyle,
    centerContainer: ViewStyle,
}

type Props = WithAuthActions &
    WithStyles<Styles, ThemeStyles> &
    NavigationScreenProps<{}> &
    OwnProps




const styles = (theme: Theme): Styles => ({
    submitErrorMsg: {
        color: theme.colors.error
    },
    centerContainer: {
        marginTop: 20,
        flexDirection: "row",
        justifyContent: 'center',
    }
})

class PasswordRecovery extends React.PureComponent<Props, {}>{

    async recoveryPassowrd(email: string) {
        try {
            await this.props.recoveryPassword(email);
            return null;
        } catch (e) {//SubmissionError
            return e.validationErrors
        }
    }

    render() {
        const {
            auth,
            styles
        } = this.props;
        return (
            <ContainerWithLoader
                showLoader={auth.loading}
                style={{ padding: 10 }}
            >
                <Form
                    onSubmit={async (values: any) => {
                        return await this.recoveryPassowrd(values.email);
                    }}
                    render={({ handleSubmit, values }) => (
                        <View>
                            <Field
                                name={'email'}
                                render={(fieldProps) => {
                                    return (
                                        <TextField
                                            fieldProps={fieldProps}
                                            placeholder={"Email"}
                                        />
                                    )
                                }}
                                validate={FormHelper.validate([
                                    new Require({ msg: 'Необходимо заполнить поле "E-mail"' }),
                                    new Email({ msg: "Введенный е-mail не корректен" })
                                ])}
                            />
                            <View style={{ paddingHorizontal: 60 }}>
                                <Button
                                    title="Восстановить"
                                    onPress={() => handleSubmit()}
                                />
                            </View>
                        </View>
                    )}
                />
                <View style={[styles.centerContainer]}>
                    <Text style={{ color: 'black' }}>Вернуться к </Text>
                    <Text style={{ color: 'blue' }}
                        onPress={() => this.props.navigation.goBack()}>
                        Авторизации </Text>
                </View>
            </ContainerWithLoader>
        )
    }
}

const mapStateToProps = (state: AppState) => {
    return {
        auth: state.auth
    }
}

export default compose(
    withStyles<Styles, Theme>(styles),
    connect(mapStateToProps, AuthActions)
)(PasswordRecovery);