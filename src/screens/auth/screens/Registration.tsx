import * as React from "react";
import { View, Text, Button, StyleSheet, TextStyle, ViewStyle } from "react-native";
import { NavigationScreenProps } from "react-navigation";
import { Form, Field } from "react-final-form";
import TextField from "../../../components/fields/TextField";
import FormHelper from "../../../utils/FormHelper";
import { Require, Phone, Email, Range } from "../../../utils/validators";
import ContainerWithLoader from "../../../components/ContainerWithLoader";
import { connect } from "react-redux"
import { AppState } from "../../../store/reducers";
import { AuthState } from "../../../store/reducers/AuthReducer";
import { WithAuthActions, RegisterValues } from "../../../store/actions/AuthActions";
import { Theme, ThemeStyles } from "../../../assets/styles/theme";
import { WithStyles } from "react-native-theme-provider";
import compose from "react-hoc-compose";
import { withStyles } from "react-native-theme-provider";

interface IOwnProps {
    auth: AuthState
}

interface Styles {
    submitErrorMsg: TextStyle,
    centerContainer: ViewStyle,
}

type Props = WithAuthActions &
    NavigationScreenProps<{}> &
    WithStyles<Styles, ThemeStyles> &
    IOwnProps


const styles = (theme: Theme): Styles => ({
    submitErrorMsg: {
        color: theme.colors.error
    },
    centerContainer: {
        marginTop: 20,
        flexDirection: "row",
        justifyContent: 'center',
    }
})

class Registration extends React.PureComponent<Props, {}>{

    async signUp(values: RegisterValues) {
        try {
            await this.props.singup(values);
            return null;
        } catch (e) {//SubmissionError
            return e.validationErrors
        }
    }

    render() {

        const {
            auth,
            styles
        } = this.props;
        return (
            <ContainerWithLoader
                showLoader={auth.loading}
            >
                <Form
                    onSubmit={async (values: any) => {
                        return await this.signUp({ ...values });
                    }}
                    render={({ handleSubmit, form, submitError }) => {
                        return (
                            <View>
                                <Field
                                    name={'fio'}
                                    render={(fieldProps) => {
                                        return (
                                            <TextField
                                                fieldProps={fieldProps}
                                                containerProps={{
                                                    style: {
                                                        marginTop: 20
                                                    }
                                                }}
                                                placeholder={"ФИО"}
                                            />
                                        )
                                    }}
                                    validate={FormHelper.validate([
                                        new Require({ msg: 'Необходимо заполнить поле "ФИО"' })
                                    ])}
                                />
                                <Field
                                    name={'phone'}
                                    render={(fieldProps) => {
                                        return (
                                            <TextField
                                                fieldProps={fieldProps}
                                                containerProps={{
                                                    style: {
                                                        marginTop: 20
                                                    }
                                                }}
                                                placeholder={"Телефон"}
                                            />
                                        )
                                    }}
                                    validate={FormHelper.validate([
                                        new Require({ msg: 'Необходимо заполнить поле "Телефон"' }),
                                        new Phone({ msg: "Введенный номер телефона не корректен" })
                                    ])}
                                />
                                <Field
                                    name={'email'}
                                    render={(fieldProps) => {
                                        return (
                                            <TextField
                                                fieldProps={fieldProps}
                                                containerProps={{
                                                    style: {
                                                        marginTop: 20
                                                    }
                                                }}
                                                placeholder={"Email"}
                                            />
                                        )
                                    }}
                                    validate={FormHelper.validate([
                                        new Require({ msg: 'Необходимо заполнить поле "E-mail"' }),
                                        new Email({ msg: "Введенный е-mail не корректен" })
                                    ])}
                                />
                                <Field
                                    name={'password'}
                                    render={(fieldProps) => {
                                        return (
                                            <TextField
                                                fieldProps={fieldProps}
                                                containerProps={{
                                                    style: {
                                                        marginTop: 20
                                                    }
                                                }}
                                                placeholder={"Пароль"}
                                            />
                                        )
                                    }}
                                    validate={FormHelper.validate([
                                        new Require({ msg: 'Необходимо заполнить поле "Пароль"' }),
                                        new Range({ msg: "Пароль должен быть не менее 6 символов", min: 6 })
                                    ])}
                                />
                                <View style={{ paddingHorizontal: 60 }}>
                                    <Button
                                        title="Зарегистрироваться"
                                        onPress={() => handleSubmit()}
                                    />
                                </View>

                                {
                                    (!!submitError) &&
                                    <Text
                                        style={styles.submitErrorMsg}>{submitError}
                                    </Text>
                                }

                            </View>
                        )
                    }}
                />


                <View style={[styles.centerContainer]}>
                    <Text style={{ color: 'black' }}>Уже зарегистрированы? </Text>
                    <Text style={{ color: 'blue' }}
                        onPress={() => this.props.navigation.goBack()}>
                        Авторизации </Text>
                </View>

            </ContainerWithLoader>
        )
    }
}

const mapStateToProps = (state: AppState) => {
    return {
        auth: state.auth
    }
}
export default compose(
    withStyles<Styles, Theme>(styles),
    connect(mapStateToProps)
)(Registration);