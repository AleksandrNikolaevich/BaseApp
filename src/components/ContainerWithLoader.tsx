import * as React from "react";
import {View, StyleSheet, ActivityIndicator, ViewStyle} from "react-native";
import { withStyles, WithStyles } from "react-native-theme-provider";
import { Theme } from "../assets/styles/theme";
import compose from "react-hoc-compose";

interface Styles {
    container: ViewStyle;
    loaderContainer: ViewStyle;
}

const styles = (theme: Theme): Styles=>({
    container:{
        flex:1,
        position:'relative',
    },
    loaderContainer:{
        position: 'absolute',
        top:0,
        left: 0,
        bottom: 0,
        right: 0,
        flex:1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(255, 255, 255, .6)'
    }
});

type OwnProps = {
    showLoader: boolean,
    hideContent?: boolean,
    style?: ViewStyle
}
type ContainerWithLoaderProps = WithStyles<Styles, Theme> & OwnProps

const ContainerWithLoader: React.StatelessComponent<ContainerWithLoaderProps> = (props)=>{
    const {showLoader, hideContent, children, style, styles} = props;
    return(
        <View style={[styles.container]}>
            {!hideContent && <View style={style}>{children}</View>}
            {
                showLoader &&  <View style={styles.loaderContainer}>
                    <ActivityIndicator/>
                </View>
            }
        </View>
    )
};


ContainerWithLoader.defaultProps={
    hideContent: false,
    style: {}
};
//для withStyles обязательно укаываем типы Styles, Theme
//для compose обязательно указываем тип своих свойств компонента OwnProps
export default compose(withStyles<Styles, Theme>(styles))<OwnProps>(ContainerWithLoader);

