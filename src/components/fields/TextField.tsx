import * as React from "react";
import {
    TextInput,
    StyleSheet,
    Platform,
    View,
    Text,
    Image,
    TouchableOpacity,
    ViewProps,
    TextInputProps,
    TextStyle
} from "react-native"
import { withStyles, WithStyles } from "react-native-theme-provider";
import { Theme } from "../../assets/styles/theme";
import { TFieldRenderProps } from "../../types";

interface Styles {
    textInput: TextStyle,
    errorMsg: TextStyle
}

const styles = (theme: Theme): Styles=> ({
    textInput: {
        paddingVertical: Platform.select({ android: 5, ios: 5 }),
        marginTop: 0,
        padding: 0,
        flex: 1
    },
    errorMsg: {
        ...theme.fonts.caption,
        color: theme.colors.error
    },
});

type TextFieldProps = TextInputProps & WithStyles<Styles, Theme> & {
    fieldProps: TFieldRenderProps,
    containerProps?: ViewProps,
    additional?: {
        image: any
    }
}

class TextField extends React.PureComponent<TextFieldProps, {}> {
    render() {
        const {
            fieldProps: {
                input,
                meta
            },
            containerProps,
           styles,
            ...inputProps
        } = this.props;
        return (
            <View {...containerProps}>
                <View style={[
                    {
                        borderColor: 'gray',
                        borderBottomWidth: 1, flexDirection: 'row'
                    },
                    input.value && { borderColor: 'black' },
                    meta.active && { borderColor: 'blue' },
                    (meta.touched && (meta.error || meta.submitError)) && { borderColor: 'red' },
                ]}>
                    <TextInput
                        {...inputProps}
                        onFocus={(e: any) => input.onFocus(e)}
                        onBlur={(e: any) => input.onBlur(e)}
                        style={[
                            styles.textInput,
                            inputProps.style 
                        ]}
                        value={input.value}
                        onChangeText={input.onChange}
                        underlineColorAndroid="transparent"
                    />
                    {/* image &&
                    <View style={[{paddingVertical: 10}]}>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            onPress={()=>this.nameInput.focus()}>
                        <Image source={image} style={{width: 18, height: 18}}/>
                        </TouchableOpacity>
                    </View> */
                    }
                </View>
                {(meta.touched && (!!meta.error || !!meta.submitError)) && <Text style={styles.errorMsg}>{meta.error || meta.submitError}</Text>}
            </View>
        );

    }
}

export default withStyles<Styles, Theme>(styles)(TextField);
