import * as React from 'react';
import {
    Text as TextOrigin,
    TextProperties
} from 'react-native';

export default class Text extends React.PureComponent<TextProperties, {}>{
    render(){
        return(
            <TextOrigin {...this.props} allowFontScaling = {this.props.allowFontScaling || false}>{this.props.children}</TextOrigin>
        )
    }
}