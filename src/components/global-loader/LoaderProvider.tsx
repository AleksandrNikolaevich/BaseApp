import * as React from "react";
import * as PropTypes from "prop-types"
import {
    View
} from "react-native"
import DefaultLoader from "./DefaultLoader";

type Props = {
    loader?: () => React.ReactNode
}

type State = {
    loading: { [idLoader: string]: boolean },
}

class LoaderProvider extends React.Component<Props, State>{
    state = {
        loading: {},
    }

    public static childContextTypes = {
        setLoaderState: PropTypes.func
    };

    getChildContext() {
        return {
            setLoaderState: (id: string, state: boolean) => {
                this.setState((prevState) => ({
                    loading: {
                        ...prevState.loading,
                        [id]: state
                    }

                }))
            }
        };
    }

    render() {
        const {
            loading,
        } = this.state;
        const { loader } = this.props;
        const showLoader = Object.values(loading).find(item => !!item);

        const LoaderComponent = loader || function(){return(<DefaultLoader/>)}

        return (
            <View
                style={{
                    position: 'relative',
                    display: "flex",
                    flexDirection: 'column',
                    flex: 1,
                    width: '100%'
                }}
            >
                {this.props.children}
                {
                    !!showLoader && LoaderComponent()
                }
            </View>
        )

    }
}

export default LoaderProvider