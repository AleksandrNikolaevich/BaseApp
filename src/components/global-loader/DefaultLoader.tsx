import * as React from "react";
import { View, ActivityIndicator, StyleSheet } from "react-native";

const styles = StyleSheet.create({
    loaderContainer:{
        position: 'absolute',
        top:0,
        left: 0,
        bottom: 0,
        right: 0,
        flex:1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(255, 255, 255, .6)'
    }
})

const DefaultLoader = () => {
    return (
        <View style={styles.loaderContainer}>
            <ActivityIndicator />
        </View>
    )
}

export default DefaultLoader;