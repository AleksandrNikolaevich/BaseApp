import * as React from "react";
import * as PropTypes from "prop-types";

type Props = {
    loading: Boolean
}

function makeid()
{
    var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyz";

    for( var i=0; i < 8; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

class Loader extends React.PureComponent<Props, {}>{
    id: string = makeid()
    public static contextTypes = {
        setLoaderState: PropTypes.func
    };

    componentDidMount(){
        this.context.setLoaderState(this.id, this.props.loading);
    }

    componentWillUnmount(){
        this.context.setLoaderState(this.id, false);
    }

    componentDidUpdate(prevProps:Props){
        if(this.props.loading !== prevProps.loading){
            this.context.setLoaderState(this.id, this.props.loading);
        }
    }

    render(){ return <div/>};
}
export default Loader;