import * as React  from "react";
import { NavigationScreenProps } from "react-navigation";
import { View, ViewStyle } from "react-native";
import compose from "react-hoc-compose";
import { withStyles, WithStyles } from "react-native-theme-provider";
import { Theme } from "../assets/styles/theme";

interface Styles {
    root: ViewStyle
}

const styles = (theme: Theme): Styles => ({
    root:{
        ...theme.styles.rootContainer
    }
})

type OwnProps = {

}

type Props = NavigationScreenProps 
    & WithStyles<Styles, Theme> 
    & OwnProps

class Catalog extends React.PureComponent<Props,{}>{
    render(){
        return(
            <View style={this.props.styles.root}>
                {this.props.children}
            </View>
        )
    }
}

export default compose(
    withStyles<Styles, Theme>(styles)
)(Catalog)