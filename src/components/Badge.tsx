import React, { StatelessComponent } from "react";
import PropTypes from "prop-types";
import {
    View,
    Text,
    StyleSheet,
    ViewStyle,
    TextStyle
} from "react-native"
import { withStyles, WithStyles } from "react-native-theme-provider";
import { Theme } from "../assets/styles/theme";

interface Styles {
    root: ViewStyle,
    badge: ViewStyle,
    text: TextStyle
}

const styles = (theme: Theme): Styles =>({
    root: {
        position: "relative"
    },
    badge:{
        position: 'absolute',
        minWidth: 16,
        minHeight: 16,
        borderRadius: 9,
        backgroundColor: theme.colors.error,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 1000
    },
    text:{
        ...theme.fonts.caption,
        color: 'white',
        fontSize: 12,
    }
})

type Props = WithStyles<Styles, Theme> & {
    value: string | number,
    hide?: boolean,
    right?: number,
    top?: number
}

const Badge:StatelessComponent<Props> = (props) => {

    const { children, value, hide, right, top, styles } = props;
    return (
        <View style={styles.root}>
            {children}
            {
                !hide &&
                <View style={[styles.badge, {right, top}]}>
                    <Text style={styles.text}>
                        {value}
                    </Text>
                </View>
            }
        </View>
    )
}


Badge.defaultProps = {
    hide: false,
    right: -10,
    top: -5
}

export default withStyles<Styles, Theme>(styles)(Badge);