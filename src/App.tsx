
import * as React from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider, connect } from 'react-redux';
import AppNavigator, {Navigator} from './navigators/AppNavigator';
import { middleware } from './utils/redux';
import Reducers, { AppState } from './store/reducers';
import { NotifiacationProvider } from 'react-native-app-notification';
import { ThemeProvider } from 'react-native-theme-provider';
import DefaultTheme from './assets/styles/DefaultTheme';
import LoaderProvider from './components/global-loader/LoaderProvider';
import gesture from "react-native-gesture-handler"

//сосдаем глобальный стейт и коннектим мидлверы(расширение функционала)
const store = createStore(
    Reducers,
    applyMiddleware(...middleware),
);

type Props = {};
export default class App extends React.Component<Props> {
    render() {
        return (
            <Provider store={store}>
                <ThemeProvider
                    themes={{
                        default: DefaultTheme
                    }}
                >
                    <NotifiacationProvider
                    /* colors={{
                        error: "red",
                        success: "geen",
                        info: "blue",
                        warn: "yellow"
                        
                    }} */
                    >
                        <LoaderProvider>
                            <AppNavigator />
                        </LoaderProvider>
                    </NotifiacationProvider>
                </ThemeProvider>
            </Provider>
        );
    }
}