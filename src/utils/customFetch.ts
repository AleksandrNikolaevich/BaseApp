import axios, { AxiosInstance } from "axios";
export type TFetch = {
    fetch: AxiosInstance,
    authorizeApi: (token?: string) => void,
    setBaseUrl: (baseURL: string) => void
}

const fetch = axios.create({
    responseType: "json",
    withCredentials: true,
    timeout: 30000,
    headers: {
        'Cache-Control': 'no-cache'
    }
});

fetch.interceptors.response.use(
    (response) => {
        return response.data;
    },
    (error) => {
        return Promise.reject(error);
    });

const authorizeApi = (token?: string) => {
    fetch.defaults.headers.common.Authorization = token ? "Bearer " + token : null;
};

const setBaseUrl = (baseURL: string) => {
    axios.defaults.baseURL = baseURL;
};

const api: TFetch = {
    fetch,
    authorizeApi,
    setBaseUrl
};



export default api