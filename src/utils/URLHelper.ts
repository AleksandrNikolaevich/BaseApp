export default class URLHelper{
    static buildUrl(url: string, params: {[x: string]: string | number}){
        Object.keys(params).forEach(key=>{
            url = url.replace(`{${key}}`, `${params[key]}`)
        })
        return url;
    }
}