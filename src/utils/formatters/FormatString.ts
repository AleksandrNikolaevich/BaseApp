import { IFormatter } from "../FormHelper";

export default class FormatString implements IFormatter{
    format: string;

    constructor(format: string){
        this.format = format;
    }

    parse(value:string): string{
        return value;
    }
}