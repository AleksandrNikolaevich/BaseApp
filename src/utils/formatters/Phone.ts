import FormatString from "./FormatString";
//@ts-ignore
import formatString from "format-string-by-pattern";

export default class Phone extends FormatString{
    constructor(){
        super('(999)999-99-99')
    }

    parse(value: string):string{
        value = value.replace('+7', '');
        return '+7' + formatString(this.format)(value)
    }
}