

const LaunchState = {
    pending: "pending",
    launching: "launching",
    done: "done"
}

export default LaunchState