type TNetworkStatusLoading = 1
type TNetworkStatusRefetch = 2
type TNetworkStatusLoadMore = 3
type TNetworkStatusReady = 4
export declare type TNetworkStatus = TNetworkStatusLoading | TNetworkStatusRefetch | TNetworkStatusLoadMore | TNetworkStatusReady

type NetworkStatusEnum = {
    loading: TNetworkStatusLoading,
    refetch: TNetworkStatusRefetch,
    loadMore: TNetworkStatusLoadMore,
    ready: TNetworkStatusReady
}

const NetworkStatus: NetworkStatusEnum = {
    loading: 1,
    refetch: 2,
    loadMore: 3,
    ready: 4
}

export default NetworkStatus