import ExtendableError from 'es6-error';

export type ValidatonErrors = {
    [x: string]:string
}

class SubmissionError extends ExtendableError {
    validationErrors: ValidatonErrors;
    constructor(validationErrors: ValidatonErrors) {
        super("Submit failed");
        this.validationErrors = validationErrors;
    }
}

export default SubmissionError;