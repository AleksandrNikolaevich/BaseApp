import { AsyncStorage } from "react-native";

export interface LatLng {
    latitude: number;
    longitude: number;
}

const addressesKey = 'addresses'

export default class Geocoder{
    static async getCoords(address: string): Promise<string> {
        const coords = await Geocoder.findDataBase(address);
        if(coords){
            return coords;
        }
        return await fetch(`https://geocode-maps.yandex.ru/1.x/?geocode=${address}&format=json`, {
            method: 'GET'
        }).then(async res => {
            if(res.status === 200){
                const result = await res.json();
                const geoCollection = result.response.GeoObjectCollection.featureMember
                if (geoCollection.length > 0) {
                    const coords = geoCollection[0].GeoObject.Point.pos.split(' ').reverse().join(',');
                    Geocoder.addPoint(address, coords);
                    return coords
                }
            }
        })
    }

    static async findDataBase(address: string){
        const data = await AsyncStorage.getItem(addressesKey);
        if(data){
            const dataBase = JSON.parse(data);
            return dataBase[address]
        }
        return undefined
    }


    static async addPoint(address: string, coords: string){
        const data = await AsyncStorage.getItem(addressesKey);
        let dataBase = {[address]: coords};
        if(data){
            dataBase = JSON.parse(data);
            dataBase[address] = coords;
        }
        await AsyncStorage.setItem(addressesKey, JSON.stringify(dataBase));
    }

    static parseCoordinate = (coords: string): LatLng => {
        const arr = coords.split(',');
        return {
            latitude: parseFloat(arr[0]),
            longitude: parseFloat(arr[1])
        };
    };
}