import { Validator } from "../FormHelper";
import Pattern from "./Pattern";

type EmailValidatorInitValue = {
    msg: string
}

class Email extends Pattern{
    constructor({msg}:EmailValidatorInitValue){
        super({msg, pattern: /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/})
    }
}

export default Email;