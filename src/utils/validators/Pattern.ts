import { Validator } from "../FormHelper";

type PatternValidatorInitValue = {
    pattern: RegExp, 
    msg: string
}

class Pattern implements Validator{
    msg: string = '';
    pattern:RegExp = /\.{0,}/g;
    constructor({pattern, msg}:PatternValidatorInitValue){
        this.msg = msg;
        this.pattern = pattern;
    }

    validate(value: string){
        if(!this.pattern.test(value)){
            return this.msg;
        }
        return undefined;
    }
}

export default Pattern;