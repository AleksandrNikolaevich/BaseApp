import Require from "./Require";
import Email from "./Email";
import Pattern from "./Pattern";
import Range from "./Range";
import Phone from "./Phone";
export {
    Require,
    Phone,
    Email,
    Pattern,
    Range
}