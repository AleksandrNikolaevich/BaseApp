import { Validator } from "../FormHelper";

type RequireValidatorInitValue = {
    msg: string
}

class Require implements Validator{
    msg: string = '';
    constructor({msg}:RequireValidatorInitValue){
        this.msg = msg;
    }

    validate(value: string){
        if(!value || value === null || value === ''|| value === undefined){
            return this.msg;
        }
        return undefined;
    }
}

export default Require;