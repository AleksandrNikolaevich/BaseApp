import { Validator } from "../FormHelper";
type RangeValidatorInitValue = {
    min?: number, 
    max?: number,
    msg: string
}

class RangeValidator implements Validator{
    msg: string = '';
    min?: number;
    max?: number;

    constructor({min, max, msg}: RangeValidatorInitValue){
        if(min && min < 0){
            throw Error('RangeValidator: Минимальное значение не может быть меньше 0')
        }
        if(min === undefined && max === undefined){
            throw Error('RangeValidator: Вы не указали ни максимальное, ни минимальное значение')
        }
        this.msg = msg;
        this.min = min;
        this.max = max;
    }

    validate(value: string){
        if(this.min && value.length < this.min){
            return this.msg;
        }
        if(this.max && value.length > this.max){
            return this.msg;
        }
        if(this.min && this.max &&  (value.length < this.min || value.length > this.max)){
            return this.msg;
        }
        return undefined;
    }
}

export default RangeValidator;