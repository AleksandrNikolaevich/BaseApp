import { Validator } from "../FormHelper";
import Pattern from "./Pattern";

type PhoneValidatorInitValue = {
    msg: string
}

class Phone extends Pattern{
    constructor({msg}:PhoneValidatorInitValue){
        super({msg, pattern: /^(^(8|\+7)+([0-9]{10}))|^([0-9]{6,10})$/})
    }
}

export default Phone;