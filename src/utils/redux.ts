//create middleware

import thunkMiddleware from 'redux-thunk';
import api, { TFetch } from './customFetch';


export type ThunkExtra = {
    api: TFetch
}

export const middleware = [
    thunkMiddleware.withExtraArgument({
        api
    }),
];