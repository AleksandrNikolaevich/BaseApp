// import axios from "axios";

// const api = axios.create({
//     baseURL: BASE_URL,
//     responseType: "json",
//     withCredentials: true,
//     timeout: 30000,
//     headers: {
//         'Cache-Control': 'no-cache'
//     }
// });

// api.interceptors.response.use(
//     (response) => {
//         return response.data;
//     },
//     (error) => {
//         return Promise.reject(error);
//     });

// const authorizeApi = (token = null) => {
//     api.defaults.headers.common.Authorization = token ? "Bearer " + token : null;
// };

// export { api, authorizeApi };

export type Api = {
    [x: string]: string
}

export const api: Api = {
    login:'auth/login/',
    signup:'auth/signup/',
    recoveryPassword:'auth/recovery/',
};
