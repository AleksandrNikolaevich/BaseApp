import {api, Api} from "./api"

export type Config = {
    appName: string,
    desctiptionApp: string,
    baseUrl: string,
    version: string,
    api: Api
}

export const config: Config = {
    appName: 'Base application',
    desctiptionApp: 'demonstration app',
    baseUrl:'http://api.aqua-delivery.ru/v1/',
    version:'0.0.1',
    api
};