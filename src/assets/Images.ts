

export const Images = {
    logo: require("./images/logo.png"),
    mainNavigation: {
        catalog:{
            active: require("./images/main-navigation/active/catalog.png"),
            inactive: require("./images/main-navigation/inactive/catalog.png"),
        },
        history:{
            active: require("./images/main-navigation/active/history.png"),
            inactive: require("./images/main-navigation/inactive/history.png"),
        },
        promotions:{
            active: require("./images/main-navigation/active/promotions.png"),
            inactive: require("./images/main-navigation/inactive/promotions.png"),
        }
    }
}