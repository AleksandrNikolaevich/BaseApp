import { Theme } from "./theme";

export const colors = {
    primary: '#4671ff',
    light: '#87a0fe',
    success: '#0cd8ab',
    error: '#ff426b',
    inactiveText: 'rgba(255, 255, 255, .5)',
    header: 'rgb(32, 32, 35)',
    background: 'white',
    footer: 'rgb(43, 44, 51)'
}

const DefaultTheme: Theme = {
    name: 'default',
    colors,
    styles: {
        rootContainer: {
            flex: 1,
            backgroundColor: colors.background
        }
    },
    fonts: {
        headline: {
            fontSize: 22,
            fontWeight: '300'
        },
        title: {
            fontSize: 20,
            fontWeight: '600',
        },
        subheading: {
            fontSize: 18,
            fontWeight: '300',
        },
        body: {
            fontSize: 16,
            fontWeight: '300',
        },
        body2: {
            fontSize: 16,
            fontWeight: '600',
        },
        caption: {
            fontSize: 14,
            fontWeight: '300',
        }
    },
    shadows: {
        light: {

        }
    }
}

export default DefaultTheme