import { ViewStyle } from "react-native";
import { ThemeBase } from "react-native-theme-provider";

type Colors = "primary"
    | "light"
    | "success"
    | "error"
    | "inactiveText"
    | "header"
    | "background"
    | "footer"

type Fonts = "headline" | "title" | "subheading" | "caption" | "body" | "body2";
/*  Display2
    Display1
    Headline
    Title
    Subheading
    Body2
    Body1
    Caption
} */
type Shadows = "light";

export interface ThemeStyles {
    rootContainer: ViewStyle
}

export type Theme = ThemeBase<ThemeStyles, Colors, Fonts, Shadows>;