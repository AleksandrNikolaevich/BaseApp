import { FieldRenderProps } from "react-final-form";

//тут описываем типы

export declare type TLaunchState = "pending" | "launching" | "done"

export declare type TFieldRenderProps = FieldRenderProps<any, any>